package co.com.chourcair.ChouReto.runners;


import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/ir_a_ser_chourcair.feature",
glue ="co.com.chourcair.ChouReto.stepdefinitions",
snippets = SnippetType.CAMELCASE)
public class IrASerChourcair {
}
