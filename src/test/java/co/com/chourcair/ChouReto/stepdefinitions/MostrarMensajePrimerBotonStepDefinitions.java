package co.com.chourcair.ChouReto.stepdefinitions;


import co.com.chourcair.ChouReto.tasks.MostrarMensajePrimerBotonReto;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class MostrarMensajePrimerBotonStepDefinitions {

    @When("^el usuario presiona el primer boton para ir al portal de empleos de magneto$")
    public void elUsuarioPresionaElPrimerBotonParaIrAlPortalDeEmpleosDeMagneto() {
        theActorInTheSpotlight().attemptsTo(MostrarMensajePrimerBotonReto.showmesaje());
    }


    @Then("^al usuario se le muestra un mensaje antes de ir al portal de empleos de Magneto$")
    public void alUsuarioSeLeMuestraUnMensajeAntesDeIrAlPortalDeEmpleosDeMagneto() {

    }
}
