package co.com.chourcair.ChouReto.stepdefinitions;

import co.com.chourcair.ChouReto.tasks.IrASerChourcairReto;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class IrASerChourcairStepDefinitions {



    @When("^el usuario presiona el boton que es ser chourcair$")
    public void elUsuarioPresionaElBotonQueEsSerChourcair() {
        theActorInTheSpotlight().attemptsTo(IrASerChourcairReto.gotoserchourcair());
    }


    @Then("^el usuario es llevado a la seccion ser chourcair$")
    public void elUsuarioEsLlevadoALaSeccionSerChourcair() {

    }
}
