package co.com.chourcair.ChouReto.stepdefinitions;


import co.com.chourcair.ChouReto.tasks.VolverAlHomeChourReto;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;


public class VolverAlHomeStepDefinitions {

    @Managed(driver = "chrome")
    WebDriver driver;

    @Before
    public void setThestage(){
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled("German");
    }

    @Given("^el usuario esta en la seccion de empleos$")
    public void elUsuarioEstaEnLaSeccionDeEmpleos() {
        theActorInTheSpotlight().wasAbleTo(Open.url("https://www.choucairtesting.com/empleos-testing/"));
    }

    @When("^el usuario presiona el logo de chorcair$")
    public void elUsuarioPresionaElLogoDeChorcair() {
        theActorInTheSpotlight().attemptsTo(VolverAlHomeChourReto.backtohome());

    }

    @Then("^el ussuario vuelve al home$")
    public void elUssuarioVuelveAlHome() {

    }



}
