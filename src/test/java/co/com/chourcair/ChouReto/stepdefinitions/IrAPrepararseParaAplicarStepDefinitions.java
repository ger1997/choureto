package co.com.chourcair.ChouReto.stepdefinitions;

import co.com.chourcair.ChouReto.tasks.IrAPrepararseParaAplicarReto;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class IrAPrepararseParaAplicarStepDefinitions {

    @When("^el usuario presiona el boton prepararse para aplicar$")
    public void elUsuarioPresionaElBotonPrepararseParaAplicar() {
        theActorInTheSpotlight().attemptsTo(IrAPrepararseParaAplicarReto.gotoprepare());
    }


    @Then("^el usuario es llevado a la seccion prepararse para aplicar$")
    public void elUsuarioEsLlevadoALaSeccionPrepararseParaAplicar() {

    }


}
