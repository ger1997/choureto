package co.com.chourcair.ChouReto.stepdefinitions;

import co.com.chourcair.ChouReto.tasks.IrAlPortalMagnetoPrimeroBotonReto;
import co.com.chourcair.ChouReto.tasks.MostrarMensajePrimerBotonReto;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class IrAlPortalMagnetoPrimerBotonStepDefinitions {

    @When("^y presiona el boton continuar del mensaje$")
    public void yPresionaElBotonContinuarDelMensaje() {
        theActorInTheSpotlight().attemptsTo(IrAlPortalMagnetoPrimeroBotonReto.gotoportalmagnetofirtsbutton());
    }


    @Then("^el usuario es llevado al portal de empleos de magneto$")
    public void elUsuarioEsLlevadoAlPortalDeEmpleosDeMagneto() {

    }


}
