package co.com.chourcair.ChouReto.stepdefinitions;


import co.com.chourcair.ChouReto.tasks.MostrarMensajeSegundoBotonReto;
import cucumber.api.java.en.When;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class MostrarMensajeSegundoBotonStepDefinitions {

    @When("^el usuario presiona el segundo boton para ir al portal de empleos de magneto$")
    public void elUsuarioPresionaElSegundoBotonParaIrAlPortalDeEmpleosDeMagneto() {
        theActorInTheSpotlight().attemptsTo(MostrarMensajeSegundoBotonReto.showmesaje());
    }
}
