package co.com.chourcair.ChouReto.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class MostrarMensajeSegundoBoton {

    public static final Target MOSTRARMENSAJESEGUNDOBOTON = Target.the(
            "mostrar mensaje segundo boton").
            locatedBy("(//a[@class=\"elementor-button-link elementor-button elementor-size-lg\"])[2]");
}
