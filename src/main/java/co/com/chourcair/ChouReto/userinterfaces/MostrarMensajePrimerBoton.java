package co.com.chourcair.ChouReto.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class MostrarMensajePrimerBoton {

    public static final Target MOSTRARMENSAJEPRIMERBOTON = Target.the(
            "mostrar mensaje primer boton").
            locatedBy("(//a[@class=\"elementor-button-link elementor-button elementor-size-lg\"])[1]");


}
