package co.com.chourcair.ChouReto.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class PrepararseParaAplicar {

    public static final Target PREPARARSE = Target.the(
            "prepararse para aplicar").
            locatedBy("//img[@title=\"empleosazulMesa de trabajo 1\"]");
}
