package co.com.chourcair.ChouReto.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class IrAlPortalMagnetoPrimerBoton {
    public static final Target IRALPORTALMAGNETOPRIMERBOTON = Target.the(
            "ir al portal magneto primer boton").
            locatedBy("//a[@class=\"elementor-button-link elementor-button elementor-size-sm\"]");

}
