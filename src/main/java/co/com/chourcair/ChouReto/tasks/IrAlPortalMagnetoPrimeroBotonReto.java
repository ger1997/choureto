package co.com.chourcair.ChouReto.tasks;

import co.com.chourcair.ChouReto.userinterfaces.IrAlPortalMagnetoPrimerBoton;
import co.com.chourcair.ChouReto.userinterfaces.MostrarMensajePrimerBoton;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.JavaScriptClick;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class IrAlPortalMagnetoPrimeroBotonReto implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                JavaScriptClick.on(IrAlPortalMagnetoPrimerBoton.IRALPORTALMAGNETOPRIMERBOTON));

    }

    public static IrAlPortalMagnetoPrimeroBotonReto gotoportalmagnetofirtsbutton(){
        return instrumented(IrAlPortalMagnetoPrimeroBotonReto.class);
    }
}
