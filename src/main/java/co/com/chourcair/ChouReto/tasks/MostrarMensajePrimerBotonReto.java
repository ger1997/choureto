package co.com.chourcair.ChouReto.tasks;


import co.com.chourcair.ChouReto.userinterfaces.MostrarMensajePrimerBoton;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.JavaScriptClick;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class MostrarMensajePrimerBotonReto implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                JavaScriptClick.on(MostrarMensajePrimerBoton.MOSTRARMENSAJEPRIMERBOTON));
    }

    public static MostrarMensajePrimerBotonReto showmesaje(){
        return instrumented(MostrarMensajePrimerBotonReto.class);
    }
}
