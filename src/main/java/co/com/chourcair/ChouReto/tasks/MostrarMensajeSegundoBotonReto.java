package co.com.chourcair.ChouReto.tasks;


import co.com.chourcair.ChouReto.userinterfaces.MostrarMensajeSegundoBoton;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.JavaScriptClick;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class MostrarMensajeSegundoBotonReto implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
        JavaScriptClick.on(MostrarMensajeSegundoBoton.MOSTRARMENSAJESEGUNDOBOTON));

    }

    public static MostrarMensajeSegundoBotonReto showmesaje(){
        return instrumented(MostrarMensajeSegundoBotonReto.class);
    }
}
