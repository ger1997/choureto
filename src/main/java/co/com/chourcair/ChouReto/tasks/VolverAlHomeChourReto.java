package co.com.chourcair.ChouReto.tasks;

import co.com.chourcair.ChouReto.userinterfaces.Empleo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class VolverAlHomeChourReto implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
               Click.on(Empleo.LOGO)

        );
    }
    public static VolverAlHomeChourReto backtohome (){
        return instrumented(VolverAlHomeChourReto.class);
    }
}
