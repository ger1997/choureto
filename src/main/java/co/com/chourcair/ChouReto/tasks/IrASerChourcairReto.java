package co.com.chourcair.ChouReto.tasks;


import co.com.chourcair.ChouReto.userinterfaces.SerChourcair;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class IrASerChourcairReto implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
         Click.on(SerChourcair.SERCHOURCAIR)

        );
    }
    public static IrASerChourcairReto gotoserchourcair () {
        return instrumented(IrASerChourcairReto.class);
    }
}
