package co.com.chourcair.ChouReto.tasks;

import co.com.chourcair.ChouReto.userinterfaces.PrepararseParaAplicar;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class IrAPrepararseParaAplicarReto implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(PrepararseParaAplicar.PREPARARSE)

        );
    }

    public static IrAPrepararseParaAplicarReto gotoprepare(){
        return instrumented(IrAPrepararseParaAplicarReto.class);
    }
}
